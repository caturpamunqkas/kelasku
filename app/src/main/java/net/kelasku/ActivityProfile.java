package net.kelasku;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.kelasku.Models.ModelGetProfile;
import net.kelasku.Utils.SharedPrefManager;
import net.kelasku.api.BaseApiService;
import net.kelasku.api.UtilsApi;

import dmax.dialog.SpotsDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ActivityProfile extends AppCompatActivity {

    BaseApiService mApiService;
    Context mContext;
    String token;
    SharedPrefManager sharedPrefManager;
    TextView profileName, profileMajors, profileEmail, bioBirthdate, bioRollno, bioJabatan, profilePhone;
    ImageView profilePict;

    AlertDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        profileName = findViewById(R.id.profileName);
        profileMajors = findViewById(R.id.profileMajors);
        profileEmail = findViewById(R.id.profileEmail);
        profilePict = findViewById(R.id.profilePict);
        profilePhone = findViewById(R.id.profilePhone);
        bioRollno = findViewById(R.id.bioRollno);
        bioBirthdate = findViewById(R.id.bioBirthdate);
        bioJabatan = findViewById(R.id.bioJabatan);

        loading = new SpotsDialog(this, R.style.Custom);
        loading.show();

        mApiService = UtilsApi.getAPIService();
        sharedPrefManager = new SharedPrefManager(this);
        token = sharedPrefManager.getSPToken();

        mApiService.getData(token).enqueue(new Callback<ModelGetProfile>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ModelGetProfile> call, @NonNull Response<ModelGetProfile> response) {
                try {
                    String name = response.body().getResults().getName();
                    String majors = response.body().getResults().getMajors();
                    String email = response.body().getResults().getEmail();
                    String urlPict = response.body().getResults().getImagedir();
                    String absent = response.body().getResults().getAbsent();
                    String birthday = response.body().getResults().getBirthday();
                    String phone = response.body().getResults().getPhone();

                    profileName.setText(name);
                    profileMajors.setText(majors+ " 2 - Absen "+ absent);
                    profileEmail.setText(email);
                    profilePhone.setText(phone);

                    bioRollno.setText(": "+absent);
                    bioBirthdate.setText(": "+birthday);

                    if (response.body().getResults().getAbsent().equals("2")){
                        bioJabatan.setText(": Ketua Kelas");
                    }

                    Picasso.with(getApplicationContext())
                            .load(urlPict)
                            .error(R.drawable.ic_boy)
                            .into(profilePict);

                    loading.dismiss();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ModelGetProfile> call, @NonNull Throwable t) {
                loading.dismiss();
                Toast.makeText(mContext, "Pastikan Anda Terhubung Dengan Internet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
